#!/usr/bin/env bash

#-------------[Start Docker configuration]--------------------------------------

dockerName="docker-popov-testing-1"
apachePort=1235
mysqlUser="iamuser"
mysqlPass="iampassword"
mysqlPort=3307
databaseName="finance"

#-------------[Finish Docker configuration]------------------------------------
UserName=${USER}
UserGroup=$(id -gn)

Bred="\033[37;1;41m"
Tred="\033[1;31m"
Bgreen="\033[37;1;42m"
Tgreen="\033[32m"
Bblue="\033[37;1;44m"
Tyellow="\033[33m"
Tsea="\033[36m"
Reset="\033[0m \033[97m"
ResetAll="\033[0m"

echo -e "$Reset"

function quit {
    separate
    echo -e "$ResetAll"

    exit 0
}

function separate {
    echo "+--------------------------------------------------------------------------------------------------+"
}

function dbDump {
    sudo docker exec -it $dockerName sh -c "mysqldump -u$mysqlUser -p$mysqlPass --routines --databases $databaseName --skip-comments > /var/www/database_dump.sql"
    sudo docker exec -it $dockerName sh -c "chown www-data:staff /var/www/database_dump.sql"
    sudo docker cp $dockerName:/var/www/database_dump.sql docker/
    sudo docker exec -it $dockerName sh -c "rm /var/www/database_dump.sql"

    sudo chown $UserName:$UserGroup docker/database_dump.sql
}

function dbImport {
    sudo docker cp docker/database_dump.sql $dockerName:/var/www/
    sudo docker exec -it $dockerName sh -c "mysql -u$mysqlUser -p$mysqlPass < /var/www/database_dump.sql"
}

function refreshApache {
    sudo docker cp docker/apache-default.conf $dockerName:/etc/apache2/sites-available/000-default.conf
    sudo docker exec -it $dockerName sh -c "apachectl restart"
}

function refreshConfig {
    sudo docker cp global.conf $dockerName:/var/www/html/public
}

if [ $UserName = "root" ]; then
        separate
        printf "|$Tred%-97s$Reset|\n" " You try run script as root. Don't do it, please!"

        quit
fi

if [ "$1" = "refresh" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Updating Apache configuration"
    separate

    refreshApache

    quit
fi

if [ "$1" = "userset" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Creating MYSQL user..."
    separate

    sudo docker exec -it $dockerName sh -c "mysql -uroot -e \"GRANT ALL PRIVILEGES ON *.* TO '$mysqlUser'@'localhost' IDENTIFIED BY '$mysqlPass';\""

    quit
fi

if [ "$1" = "dbdump" ]; then

    dbDump
    quit
fi

if [ "$1" = "reset" ]; then
    dbDump

    sudo docker system prune -a

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Stoping container..."
    separate

    echo -e "$Tsea"
    sudo docker stop $dockerName
    echo -e "$Reset"

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Remove container..."
    separate

    echo -e "$Tsea"
    sudo docker container rm $dockerName
    echo -e "$Reset"

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Remove image..."
    separate

    echo -e "$Tsea"
    sudo docker rmi -f $dockerName
    echo -e "$Reset"

    quit
fi

if [ "$1" = "connect" ]; then

    separate

    if ! sudo docker ps | grep '$dockerName' > /dev/null 2>&1; then

        printf "|$Tyellow%-97s$Reset|\n" " Docker is not running. Run docker automatically..."
        separate
        sudo docker start $dockerName
        separate
    fi


    printf "|$Tyellow%-97s$Reset|\n" " Connect to server..."
    separate

    echo -e "$ResetAll"
    sudo docker exec -it $dockerName /bin/bash
    echo -e "$Reset"

    quit
fi


if [ "$1" = "start" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Starting Docker..."
    separate

    echo -e "$ResetAll"
    sudo docker start $dockerName
    echo -e "$Reset"

    quit
fi


if [ "$1" = "stop" ]; then

    separate
    printf "|$Tyellow%-97s$Reset|\n" " Stoping Docker..."
    separate

    echo -e "$ResetAll"
    sudo docker stop $dockerName
    echo -e "$Reset"

    quit
fi

if [ "$1" = "parser" ]; then

    sudo docker start $dockerName
    sudo docker exec -it $dockerName sh -c "cd /var/www/html && php bin/console finance:parser"

    quit
fi

if [ "$1" = "test" ]; then

    sudo docker start $dockerName
    sudo docker exec -it $dockerName sh -c "cd /var/www/html && phpunit"

    quit
fi


separate
printf "|$Tyellow%-97s$Reset|\n" " Start process installation"
separate

# Check docker installation
if docker -v | grep 'Docker version' > /dev/null 2>&1; then

	printf "|%-48s|$Tgreen%48s$Reset|\n" " Check Docker" "OK "
else

    printf "|%-48s|$Tred%48s$Reset|\n" " Check Docker" "Docker not found. Please, install Docker. "
    quit
fi



separate
if sudo docker images | grep '$dockerName' > /dev/null 2>&1; then

	printf "|%-48s|$Tgreen%48s$Reset|\n" " Check image for project" "OK "

else
    printf "|%-48s|$Tred%48s$Reset|\n" " Check image for project" "Image not found "
    separate
    printf "|$Tyellow%-97s$Reset|\n" " Building of image"
    separate
    sleep 2

    echo -e "$Tsea"
    sudo docker build -t $dockerName .
    sudo docker run -d --name $dockerName -p "$apachePort:80" -p "$mysqlPort:3306" -v ${PWD}/app:/app mattrayner/lamp:latest-1604-php7

    sudo docker exec -it $dockerName sh -c "apt-get -y update && apt-get install htop && apt-get install nano && apt-get install composer"
    sudo docker exec -it $dockerName sh -c "cd /var/ && wget https://phar.phpunit.de/phpunit-6.5.phar && chmod +x phpunit-6.5.phar && mv phpunit-6.5.phar /usr/local/bin/phpunit"

    refreshApache

    sudo docker cp docker/mysql-default.conf $dockerName:/etc/mysql/mysql.conf.d/mysqld.cnf

    sudo docker exec -it $dockerName sh -c "cd /var/www/html && composer install"
    sudo chown -R $UserName:$UserGroup app/vendor
    sudo chown -R $UserName:$UserGroup app/var

    if ! sudo docker ps | grep '$dockerName' > /dev/null 2>&1; then
        echo -e "$Reset"
        separate
        printf "|$Tyellow%-97s$Reset|\n" " Docker is not running. Run docker automatically..."
        separate
        echo -e "$Tsea"

        sudo docker start $dockerName
    fi

    echo -e "$Reset"
    separate
    printf "|$Tyellow%-97s$Reset|\n" " Installation config of MYSQL. Need wait few minutes..."
    separate
    echo -e "$Tsea"

    sleep 240

    sudo docker exec -it $dockerName sh -c "mysql -uroot -e \"GRANT ALL PRIVILEGES ON *.* TO '$mysqlUser'@'%' IDENTIFIED BY '$mysqlPass';\""

    dbImport

    echo -e "$Reset"

fi



quit
