<?php

namespace Tests\AppBundle\Util;


use App\Entity\Exchange;
use App\Model\ParserModel;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;


class ParserTest extends TestCase
{
    private $model;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->model = new ParserModel();
    }

    public function testParsingCBR()
    {
        $this->caller('CBR');
    }

    public function testParsingECB()
    {
        $this->caller('ECB');
    }

    private function caller($provider)
    {
        $sourceUrl = getenv("URL_{$provider}");
        $xml = new Crawler(file_get_contents($sourceUrl));

        try {
            $result = $this->model->{"parse" . $provider}($xml);
            $this->checker($result);

        } catch (\Exception $e) {
            $this->expectException($e->getMessage());
        }
    }

    private function checker(array $result)
    {
        $this->assertEquals('array', gettype($result), "Parser returns incorrect format data");
        $this->assertEquals(true, (count($result) > 0), "Parser returns empty result");
        $this->assertEquals(Exchange::class, get_class($result[0]), "Parser returns incorrect entity class");

        if (get_class($result[0]) === Exchange::class) {
            /**
             * @var Exchange $item
             */
            foreach ($result as $item) {
                $this->assertEquals(true, !empty($item->getCurrency()), "Currency name not found");
                $this->assertEquals(true, !empty($item->getRate()), "Rate of currency {$item->getCurrency()} is empty");
                $this->assertEquals(true, !empty($item->getDate()), "Date of currency {$item->getCurrency()} is empty");
            }

        }
    }
}