<?php

namespace Tests\AppBundle\Util;


use App\Entity\RespConst;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class RequestTest extends WebTestCase
{
    private $client;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->client = static::createClient();
    }

    public function testRequestCorrectWithoutDate()
    {
        $this->client->request('GET', '/USD/RUB/5000');
        $this->preAssertSame('SUCCESS');
    }

    public function testRequestCorrectWithDate()
    {
        $this->client->request('GET', '/USD/JPY/5000/2019-02-15');
        $this->preAssertSame('SUCCESS');
    }

    public function testRequestIncorrectTo()
    {
        $this->client->request('GET', '/USD2/RUB/5000');
        $this->preAssertSame('ERROR');
    }

    public function testRequestIncorrectFrom()
    {
        $this->client->request('GET', '/USD/RUBRRR/5000');
        $this->preAssertSame('ERROR');
    }

    public function testRequestIncorrectAmount()
    {
        $this->client->request('GET', '/USD/RUB/QWE');
        $this->preAssertSame('ERROR');
    }

    public function testRequestIncorrectDate()
    {
        $this->client->request('GET', '/USD/RUB/5000/20019301');
        $this->preAssertSame('ERROR');
    }

    private function preAssertSame($correctStatus)
    {
        $response = $this->client->getResponse();

        $responseData = json_decode($response->getContent(), true);

        switch ($correctStatus) {
            case 'ERROR':
                $code = RespConst::CODE_ERROR;
                $stat = RespConst::STATUS_ERROR;
                break;

            case 'SUCCESS':
            default:
                $code = RespConst::CODE_SUCCESS;
                $stat = RespConst::STATUS_SUCCESS;
        }


        $this->assertSame($code, $response->getStatusCode());
        $this->assertSame(
            $stat,
            $responseData[RespConst::FIELD_STATUS],
            $responseData[RespConst::FIELD_MESSAGE]
        );
    }
}