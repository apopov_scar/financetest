<?php
/**
 * Created by PhpStorm.
 * User: scar
 * Date: 2/16/19
 * Time: 12:49 PM
 */

namespace App\Model;

use App\Entity\Exchange;
use Exception;
use Symfony\Component\DomCrawler\Crawler;

class ParserModel
{
    /**
     * @param Crawler $xml
     * @return array
     */
    public function parseECB(Crawler $xml) : array
    {
        $result = [];

        $xml->filterXPath('//gesmes:Envelope/default:Cube/default:Cube')->each(function (Crawler $node) use (&$result) {

            $date = $node->attr('time');

            if (!$date) {
                throw new Exception("Looks like DOM structure of source was changed");
            }

            $node->filterXPath('//default:Cube')->each(function (Crawler $n) use ($date, &$result) {

                $rate = $n->attr('rate');
                $currency = $n->attr('currency');

                if (!$rate || !$currency) {
                    return;
                }

                $exchange = new Exchange();
                $exchange->setDate($date);
                $exchange->setRate($rate);
                $exchange->setCurrency($currency);

                $result[] = $exchange;
            });

        });

        return $result;
    }

    /**
     * @param Crawler $xml
     * @return array
     * @throws Exception
     */
    public function parseCBR(Crawler $xml) : array
    {
        $date = $xml->filter('ValCurs')->attr('Date');

        if (!$date) {
            throw new Exception("Looks like DOM structure of source was changed");
        }

        $date = explode(".", $date);
        $date = implode("-", array_reverse($date));

        $result = $xml->filter('ValCurs > Valute')->each(function (Crawler $node) use ($date) {

            $rate = $node->filter('Value')->text();
            $currency = $node->filter('CharCode')->text();

            $exchange = new Exchange();
            $exchange->setDate($date);
            $exchange->setRate($rate);
            $exchange->setCurrency($currency);

            return $exchange;
        });

        return $result;
    }
}