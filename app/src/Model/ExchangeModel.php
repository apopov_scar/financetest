<?php
/**
 * Created by PhpStorm.
 * User: scar
 * Date: 2/16/19
 * Time: 3:24 PM
 */

namespace App\Model;


use App\Entity\Exchange;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExchangeModel
{
    private $doctrine;
    private $sourceName;

    /**
     * ExchangeModel constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->doctrine = $container->get('doctrine')->getManager();
        $this->sourceName = getenv('SOURCE');
    }

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @param $date
     * @return mixed
     * @throws Exception
     */
    public function process($from, $to, $amount, $date)
    {
        $frow = $this->getExchange($from, $date);
        $trow = $this->getExchange($to, $date);

        if(is_null($frow) || is_null($trow)){
            throw new Exception("Not found currency " . (is_null($frow) ? $from : $to ) . " in database!");
        }

        return round((100 / $frow->getRate() * $amount * $trow->getRate() / 100));
    }

    /**
     * @param Exchange $item
     * @return bool
     */
    public function save(Exchange $item) : bool
    {
        $repository = $this->doctrine->getRepository(Exchange::class);

        $product = $repository->findOneBy([
            'source' => $this->sourceName,
            'date' => $item->getDate(),
            'currency' => $item->getCurrency(),
        ]);

        if ($product === null) {
            $this->doctrine->persist($item);
            $this->doctrine->flush();
            return true;
        }

        return false;
    }

    /**
     * @param $currency
     * @param null $date
     * @return Exchange | null
     */
    private function getExchange($currency, $date = null)
    {
        $condition = [
            'source' => $this->sourceName,
            'currency' => $currency
        ];

        if (!is_null($date)) {
            $condition += ['date' => $date];
        }

        $repository = $this->doctrine->getRepository(Exchange::class);

        return $repository->findOneBy($condition, ['date' => 'DESC']);
    }
}