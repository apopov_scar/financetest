<?php
/**
 * Created by PhpStorm.
 * User: scar
 * Date: 2/16/19
 * Time: 10:20 AM
 */

namespace App\Command;

use App\Entity\Exchange;
use App\Model\ExchangeModel;
use App\Model\ParserModel;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParserCommand extends Command
{
    private $sourceUrl;
    private $sourceName;
    /**
     * @var ParserModel
     */
    private $parserModel;
    private $exchangeModel;

    /**
     * ParserCommand constructor.
     * @param ExchangeModel $exchangeModel
     * @param ParserModel $parserModel
     */
    public function __construct(ExchangeModel $exchangeModel, ParserModel $parserModel)
    {
        parent::__construct();

        $this->exchangeModel = $exchangeModel;
        $this->parserModel = $parserModel;
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('finance:parser');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->checkSource();

        $xml = new Crawler(file_get_contents($this->sourceUrl));
        $data = $this->parserModel->{"parse" . $this->sourceName}($xml);

        /**
         * @var Exchange $item
         */
        foreach ($data as $item) {
            $result = $this->exchangeModel->save($item);

            $output->writeln(
                $this->sourceName . "\t" .
                date_format($item->getDate(), 'Y-m-d') . "\t" .
                $item->getCurrency() . "\t" .
                $item->getRate() . "\t" .
                (!$result ? "\033[1;31mAlready exist\033[0m \033[97m" : "\033[32mSaved new row\033[0m \033[97m")
            );
        }
    }

    private function checkSource()
    {
        $this->sourceName = getenv('SOURCE');
        $this->sourceUrl = getenv("URL_{$this->sourceName}");

        if (!$this->sourceUrl) {
            throw new Exception("Source for parsing wrong! Please check config file.");
        }

        if (!method_exists($this->parserModel, "parse" . $this->sourceName)) {
            throw new Exception("Method of parsing for provider $this->sourceName doesn't exist.");
        }
    }


}