<?php
/**
 * Created by PhpStorm.
 * User: scar
 * Date: 2/17/19
 * Time: 10:48 AM
 */

namespace App\Entity;


class RespConst
{
    const FIELD_STATUS = 'status';
    const FIELD_MESSAGE = 'message';
    const FIELD_DATA = 'data';

    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    const CODE_SUCCESS = 200;
    const CODE_ERROR = 500;
}