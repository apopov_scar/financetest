<?php

/**
 * Created by Aleksei Popov.
 * User: scar
 * Date: 2/15/19
 * Time: 2:42 PM
 */

namespace App\Controller;


use App\Entity\RespConst;
use App\Model\ExchangeModel;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;


class MasterController extends AbstractController
{
    /**
     * @var ExchangeModel
     */
    private $model;

    /**
     * MasterController constructor.
     * @param ExchangeModel $model
     */
    public function __construct(ExchangeModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @param $date
     * @return JsonResponse
     */
    public function index($from, $to, $amount, $date)
    {
        try {
            $date = is_null($date) ? null : DateTime::createFromFormat('Y-m-d', $date);
            $response = $this->model->process($from, $to, (int)$amount, $date);

        } catch (\Exception $e) {
            return JsonResponse::create(
                [
                    RespConst::FIELD_STATUS => RespConst::STATUS_ERROR,
                    RespConst::FIELD_MESSAGE => $e->getMessage()
                ],
                RespConst::CODE_ERROR);
        }

        return JsonResponse::create(
            [
                RespConst::FIELD_STATUS => RespConst::STATUS_SUCCESS,
                RespConst::FIELD_MESSAGE => 'Process is successful',
                RespConst::FIELD_DATA => $response
            ],
            RespConst::CODE_SUCCESS);
    }

    /**
     * @return JsonResponse
     */
    public function showException()
    {
        return JsonResponse::create(
            [
                RespConst::FIELD_STATUS => RespConst::STATUS_ERROR,
                RespConst::FIELD_MESSAGE => 'Something wrong!'
            ],
            RespConst::CODE_ERROR);
    }
}