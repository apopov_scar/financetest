# Exchange Test

## Installation
- Clone repository to any folder
````
git clone git@bitbucket.org:apopov_scar/financetest.git
````
___
- Open bash script (worker.sh in root folder of project) and change (if it really need) configuration of Docker installer
```bash
#-------------[Start Docker configuration]--------------------------------------

dockerName="docker-popov-testing-1"     # Docker image name
apachePort=1235                         # Apache port instead 80 
mysqlUser="iamuser"                     # User of MySQL
mysqlPass="iampassword"                 # Password of MySQL
mysqlPort=3307                          # MySQL port instead 3306
databaseName="finance"                  # Database name

#-------------[Finish Docker configuration]------------------------------------
```
###### If you'll change mysql settings then you'll mast also change connection to DB in .env config of Doctrine.
```bash
DATABASE_URL=mysql://iamuser:iampassword@localhost:3307/finance
```
___  
- Install image of Docker. In command line go to root folder of project and run script: 
```bash
./worker.sh
```
###### When script will be finished, you can open project by link http://localhost:1235

## Configuration of project
In config file *{PATH_TO_PROJECT}/app/.env* you can change source of parsing and add new. Example:
```bash
SOURCE=ECB

URL_ECB=https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml
URL_CBR=https://www.cbr.ru/scripts/XML_daily.asp
```

## Use commands of worker.sh
##### Start Docker containers
###### (after installation Docker started automatically)
```bash
./worker.sh start
```
##### Stop Docker containers
```bash
./worker.sh stop
```
##### Connect to bash of Docker server (ssh)
```bash
./worker.sh connect
```
___ 
### Run parser of exchanges
```bash
./worker.sh parser
```
### Run tests of project
```bash
./worker.sh test
```
___
##### Dump database
```bash
./worker.sh dbdump
```
###### Database will be saved in *{PATH_TO_PROJECT}/docker/database_dump.sql*

## Request/Response
#### Request for calculate of exchange
``
http://localhost:1235/{currency_from}/{currency_to}/{amount}/{date}
``
##### Params
| Param name | Description | Value |
|------------|-------------|-------|
| {currency_from} | Currency from which to calculate | CZK (Required) |
| {currency_to} | The currency in which you want to get the result | GBP (Required) |
| {amount} | Amount in pennies $100 = 10000 | 10000 (Required) |
| {date} | Date of rate | YYYY-mm-dd (Optional) |

Example:
``
http://localhost:1235/CZK/GBP/10000/2019-02-15
``
___
#### Response for calculate of exchange
Response returns in JSON format
```json
{"status":"success","message":"Process is successful","data":342}
```
| Param name | Description | Value |
|------------|-------------|-------|
| status | Status of response | success/error |
| message | Message of response | Text |
| data | Calculate result of exchange | Integer value in pennies |

## Uninstall
- Run command of stop Docker containers and removing all images which belong to it.
```bash
./worker.sh reset
```
- Remove folder with project. 